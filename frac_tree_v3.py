#recursive function that draws the fractal tree

def branch(len,Rangle,Langle,alex,x,y,heading):
    if len>20:
        alex.pensize(len/10)
        alex.right(Rangle)
        alex.forward(len)
        branch(len*0.75,Rangle+random.choice(angles),Langle+random.choice(angles),alex,alex.xcor(),alex.ycor(),alex.heading())

        alex.penup()
        alex.setheading(heading)
        alex.setpos(x,y)
        alex.pendown()
       

        alex.left(Langle)
        alex.pensize(len/10)
        alex.forward(len)
        branch(len*0.75,Rangle+random.choice(angles),Langle+random.choice(angles),alex,alex.xcor(),alex.ycor(),alex.heading())

        alex.penup()
        alex.setheading(heading)
        alex.setpos(x,y)
        alex.pendown()
        
        alex.pensize(len/10)
        alex.right(Rangle-((Rangle+Langle)/2))
        alex.forward(len)
        branch(len*0.75,Rangle+random.choice(angles),Langle+random.choice(angles),alex,alex.xcor(),alex.ycor(),alex.heading())
        
    else:
        return
    
    




import turtle,random             # Allows us to use turtles

angles  = [5,-5]
wn = turtle.Screen()      # Creates a playground for turtles
wn.bgcolor("black")
alex = turtle.Turtle()    # Create a turtle, assign to alex

alex.pencolor("green")


Rangle=int(input("Insert right angle value: "))
Langle=int(input("Insert left angle value: "))
lenght=220
alex.hideturtle()
alex.penup()
alex.setpos(0,-500)
alex.speed("fastest")

alex.pensize(lenght/10)
alex.pendown()
alex.left(90)
alex.forward(lenght)

wn.tracer(0, 0)
branch(lenght*0.75,Rangle,Langle,alex,alex.xcor(),alex.ycor(),alex.heading())
wn.update()


