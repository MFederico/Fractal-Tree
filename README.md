# Fractal Tree

This Python program will draw a fractal tree with 3 main trunks using the Turtle graphics libray

## How to run

* Install the turtle library, and run the program
* Insert the left and right angles for the left and right trunks
* Watch the result and experiment various combinations changing the angles